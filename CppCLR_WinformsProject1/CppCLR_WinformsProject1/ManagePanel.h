﻿#pragma once

namespace CppCLR_WinformsProject1 {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Summary for ManagePanel
	/// </summary>
	public ref class ManagePanel : public System::Windows::Forms::Form
	{
	public:
		ManagePanel(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}

		ManagePanel(System::Windows::Forms::Form^ frm)
		{
			preForm = frm;
			InitializeComponent();
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~ManagePanel()
		{
			if (components)
			{
				delete components;
			}
		}

	private: System::Windows::Forms::Form^ preForm;
	private: System::Windows::Forms::Button^ bckbtn;
	private: System::Windows::Forms::Button^ button1;
	private: System::Windows::Forms::Button^ button2;

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->bckbtn = (gcnew System::Windows::Forms::Button());
			this->button1 = (gcnew System::Windows::Forms::Button());
			this->button2 = (gcnew System::Windows::Forms::Button());
			this->SuspendLayout();
			// 
			// bckbtn
			// 
			this->bckbtn->BackColor = System::Drawing::Color::Maroon;
			this->bckbtn->ForeColor = System::Drawing::Color::White;
			this->bckbtn->Location = System::Drawing::Point(45, 733);
			this->bckbtn->Name = L"bckbtn";
			this->bckbtn->Size = System::Drawing::Size(165, 68);
			this->bckbtn->TabIndex = 2;
			this->bckbtn->Text = L"خروج";
			this->bckbtn->UseVisualStyleBackColor = false;
			this->bckbtn->Click += gcnew System::EventHandler(this, &ManagePanel::bckbtn_Click);
			// 
			// button1
			// 
			this->button1->BackColor = System::Drawing::Color::Maroon;
			this->button1->ForeColor = System::Drawing::Color::White;
			this->button1->Location = System::Drawing::Point(350, 245);
			this->button1->Name = L"button1";
			this->button1->Size = System::Drawing::Size(624, 110);
			this->button1->TabIndex = 3;
			this->button1->Text = L"ساخت آزمون جدید";
			this->button1->UseVisualStyleBackColor = false;
			// 
			// button2
			// 
			this->button2->BackColor = System::Drawing::Color::Maroon;
			this->button2->ForeColor = System::Drawing::Color::White;
			this->button2->Location = System::Drawing::Point(350, 385);
			this->button2->Name = L"button2";
			this->button2->Size = System::Drawing::Size(624, 110);
			this->button2->TabIndex = 4;
			this->button2->Text = L"آزمون های موجود";
			this->button2->UseVisualStyleBackColor = false;
			// 
			// ManagePanel
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(16, 31);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->BackColor = System::Drawing::SystemColors::WindowFrame;
			this->ClientSize = System::Drawing::Size(1348, 842);
			this->Controls->Add(this->button2);
			this->Controls->Add(this->button1);
			this->Controls->Add(this->bckbtn);
			this->Name = L"ManagePanel";
			this->Text = L"ManagePanel";
			this->ResumeLayout(false);

		}
#pragma endregion
	private: System::Void bckbtn_Click(System::Object^ sender, System::EventArgs^ e) {
		preForm->Show();
		this->Hide();
	}
	};
}

﻿#pragma once
#include "StudentReg.h"
#include "StudentPanel.h"

namespace CppCLR_WinformsProject1 {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Summary for StudentLogin
	/// </summary>
	public ref class StudentLogin : public System::Windows::Forms::Form
	{
	public:
		StudentLogin(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}
		StudentLogin(System::Windows::Forms::Form^ frm)
		{
			preForm = frm;
			InitializeComponent();
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~StudentLogin()
		{
			if (components)
			{
				delete components;
			}
		}

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;
	private: System::Windows::Forms::Button^ bckbtn;
	private: System::Windows::Forms::TextBox^ passbtn;
	private: System::Windows::Forms::Label^ pass_label;
	private: System::Windows::Forms::TextBox^ userbtn;
	private: System::Windows::Forms::Label^ user_label;
	private: System::Windows::Forms::Button^ regbtn;
	private: System::Windows::Forms::Button^ Enterbtn;
	private: System::Windows::Forms::Label^ label1;
		   System::Windows::Forms::Form^ preForm;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->bckbtn = (gcnew System::Windows::Forms::Button());
			this->passbtn = (gcnew System::Windows::Forms::TextBox());
			this->pass_label = (gcnew System::Windows::Forms::Label());
			this->userbtn = (gcnew System::Windows::Forms::TextBox());
			this->user_label = (gcnew System::Windows::Forms::Label());
			this->regbtn = (gcnew System::Windows::Forms::Button());
			this->Enterbtn = (gcnew System::Windows::Forms::Button());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->SuspendLayout();
			// 
			// bckbtn
			// 
			this->bckbtn->BackColor = System::Drawing::Color::Maroon;
			this->bckbtn->ForeColor = System::Drawing::Color::White;
			this->bckbtn->Location = System::Drawing::Point(50, 734);
			this->bckbtn->Name = L"bckbtn";
			this->bckbtn->Size = System::Drawing::Size(165, 68);
			this->bckbtn->TabIndex = 0;
			this->bckbtn->Text = L"بازگشت";
			this->bckbtn->UseVisualStyleBackColor = false;
			this->bckbtn->Click += gcnew System::EventHandler(this, &StudentLogin::bckbtn_Click);
			// 
			// passbtn
			// 
			this->passbtn->Location = System::Drawing::Point(457, 329);
			this->passbtn->Name = L"passbtn";
			this->passbtn->Size = System::Drawing::Size(329, 38);
			this->passbtn->TabIndex = 13;
			// 
			// pass_label
			// 
			this->pass_label->AutoSize = true;
			this->pass_label->ForeColor = System::Drawing::Color::White;
			this->pass_label->Location = System::Drawing::Point(810, 334);
			this->pass_label->Name = L"pass_label";
			this->pass_label->Size = System::Drawing::Size(117, 32);
			this->pass_label->TabIndex = 12;
			this->pass_label->Text = L":رمز عبور";
			// 
			// userbtn
			// 
			this->userbtn->Location = System::Drawing::Point(457, 261);
			this->userbtn->Name = L"userbtn";
			this->userbtn->Size = System::Drawing::Size(329, 38);
			this->userbtn->TabIndex = 11;
			// 
			// user_label
			// 
			this->user_label->AutoSize = true;
			this->user_label->ForeColor = System::Drawing::Color::White;
			this->user_label->Location = System::Drawing::Point(810, 266);
			this->user_label->Name = L"user_label";
			this->user_label->Size = System::Drawing::Size(125, 32);
			this->user_label->TabIndex = 10;
			this->user_label->Text = L":نام کاربری";
			// 
			// regbtn
			// 
			this->regbtn->BackColor = System::Drawing::Color::Maroon;
			this->regbtn->ForeColor = System::Drawing::Color::White;
			this->regbtn->Location = System::Drawing::Point(457, 475);
			this->regbtn->Name = L"regbtn";
			this->regbtn->Size = System::Drawing::Size(329, 58);
			this->regbtn->TabIndex = 9;
			this->regbtn->Text = L"ثبت نام";
			this->regbtn->UseVisualStyleBackColor = false;
			this->regbtn->Click += gcnew System::EventHandler(this, &StudentLogin::regbtn_Click);
			// 
			// Enterbtn
			// 
			this->Enterbtn->BackColor = System::Drawing::Color::Maroon;
			this->Enterbtn->ForeColor = System::Drawing::Color::White;
			this->Enterbtn->Location = System::Drawing::Point(457, 411);
			this->Enterbtn->Name = L"Enterbtn";
			this->Enterbtn->Size = System::Drawing::Size(329, 58);
			this->Enterbtn->TabIndex = 8;
			this->Enterbtn->Text = L"ورود";
			this->Enterbtn->UseVisualStyleBackColor = false;
			this->Enterbtn->Click += gcnew System::EventHandler(this, &StudentLogin::Enterbtn_Click);
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 14, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->label1->ForeColor = System::Drawing::Color::White;
			this->label1->Location = System::Drawing::Point(530, 137);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(226, 54);
			this->label1->TabIndex = 14;
			this->label1->Text = L"ورود دانشجو";
			// 
			// StudentLogin
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(16, 31);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->BackColor = System::Drawing::SystemColors::WindowFrame;
			this->ClientSize = System::Drawing::Size(1348, 842);
			this->Controls->Add(this->label1);
			this->Controls->Add(this->passbtn);
			this->Controls->Add(this->pass_label);
			this->Controls->Add(this->userbtn);
			this->Controls->Add(this->user_label);
			this->Controls->Add(this->regbtn);
			this->Controls->Add(this->Enterbtn);
			this->Controls->Add(this->bckbtn);
			this->Name = L"StudentLogin";
			this->Text = L"StudentLogin";
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
	private: System::Void bckbtn_Click(System::Object^ sender, System::EventArgs^ e) {
		preForm->Show();
		this->Hide();
	}
	private: System::Void regbtn_Click(System::Object^ sender, System::EventArgs^ e) {
		StudentReg^ nextForm = gcnew StudentReg(this);
		nextForm->Show();
		this->Hide();
	}
private: System::Void Enterbtn_Click(System::Object^ sender, System::EventArgs^ e) {
	if (true)//age user pass ok bood
	{
		StudentPanel^ nextForm = gcnew StudentPanel(this);
		nextForm->Show();
		this->Hide();
	}
}
};
}

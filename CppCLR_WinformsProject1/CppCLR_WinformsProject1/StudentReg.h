﻿#pragma once
#include "SuccessfulReg.h"
#include "UnsuccessfulReg.h"

namespace CppCLR_WinformsProject1 {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Summary for StudentReg
	/// </summary>
	public ref class StudentReg : public System::Windows::Forms::Form
	{
	public:
		StudentReg(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}

		StudentReg(System::Windows::Forms::Form^ frm) {
			preForm = frm;
			InitializeComponent();
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~StudentReg()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::Button^ regbtn;
	protected:
	private: System::Windows::Forms::Button^ bckbtn;
	private: System::Windows::Forms::TextBox^ lastNameBox;
	private: System::Windows::Forms::Label^ last_name_label;
	private: System::Windows::Forms::TextBox^ nameBox;
	private: System::Windows::Forms::Label^ name_label;
	private: System::Windows::Forms::TextBox^ passBox;
	private: System::Windows::Forms::Label^ pass_label;
	private: System::Windows::Forms::TextBox^ userBox;
	private: System::Windows::Forms::Label^ user_label;
	private: System::Windows::Forms::Label^ label1;
	private: System::Windows::Forms::TextBox^ textBox1;
	private: System::Windows::Forms::Label^ label2;

	private: System::Windows::Forms::Form^ preForm;

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->regbtn = (gcnew System::Windows::Forms::Button());
			this->bckbtn = (gcnew System::Windows::Forms::Button());
			this->lastNameBox = (gcnew System::Windows::Forms::TextBox());
			this->last_name_label = (gcnew System::Windows::Forms::Label());
			this->nameBox = (gcnew System::Windows::Forms::TextBox());
			this->name_label = (gcnew System::Windows::Forms::Label());
			this->passBox = (gcnew System::Windows::Forms::TextBox());
			this->pass_label = (gcnew System::Windows::Forms::Label());
			this->userBox = (gcnew System::Windows::Forms::TextBox());
			this->user_label = (gcnew System::Windows::Forms::Label());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->textBox1 = (gcnew System::Windows::Forms::TextBox());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->SuspendLayout();
			// 
			// regbtn
			// 
			this->regbtn->BackColor = System::Drawing::Color::Maroon;
			this->regbtn->ForeColor = System::Drawing::Color::White;
			this->regbtn->Location = System::Drawing::Point(462, 586);
			this->regbtn->Name = L"regbtn";
			this->regbtn->Size = System::Drawing::Size(329, 58);
			this->regbtn->TabIndex = 30;
			this->regbtn->Text = L"ثبت نام";
			this->regbtn->UseVisualStyleBackColor = false;
			this->regbtn->Click += gcnew System::EventHandler(this, &StudentReg::regbtn_Click);
			// 
			// bckbtn
			// 
			this->bckbtn->BackColor = System::Drawing::Color::Maroon;
			this->bckbtn->ForeColor = System::Drawing::Color::White;
			this->bckbtn->Location = System::Drawing::Point(54, 728);
			this->bckbtn->Name = L"bckbtn";
			this->bckbtn->Size = System::Drawing::Size(165, 68);
			this->bckbtn->TabIndex = 29;
			this->bckbtn->Text = L"بازگشت";
			this->bckbtn->UseVisualStyleBackColor = false;
			this->bckbtn->Click += gcnew System::EventHandler(this, &StudentReg::bckbtn_Click);
			// 
			// lastNameBox
			// 
			this->lastNameBox->Location = System::Drawing::Point(462, 310);
			this->lastNameBox->Name = L"lastNameBox";
			this->lastNameBox->Size = System::Drawing::Size(329, 38);
			this->lastNameBox->TabIndex = 28;
			// 
			// last_name_label
			// 
			this->last_name_label->AutoSize = true;
			this->last_name_label->ForeColor = System::Drawing::Color::White;
			this->last_name_label->Location = System::Drawing::Point(815, 315);
			this->last_name_label->Name = L"last_name_label";
			this->last_name_label->Size = System::Drawing::Size(140, 32);
			this->last_name_label->TabIndex = 27;
			this->last_name_label->Text = L":نام خانوادگی";
			// 
			// nameBox
			// 
			this->nameBox->Location = System::Drawing::Point(462, 242);
			this->nameBox->Name = L"nameBox";
			this->nameBox->Size = System::Drawing::Size(329, 38);
			this->nameBox->TabIndex = 26;
			// 
			// name_label
			// 
			this->name_label->AutoSize = true;
			this->name_label->ForeColor = System::Drawing::Color::White;
			this->name_label->Location = System::Drawing::Point(815, 247);
			this->name_label->Name = L"name_label";
			this->name_label->Size = System::Drawing::Size(47, 32);
			this->name_label->TabIndex = 25;
			this->name_label->Text = L":نام";
			// 
			// passBox
			// 
			this->passBox->Location = System::Drawing::Point(462, 446);
			this->passBox->Name = L"passBox";
			this->passBox->Size = System::Drawing::Size(329, 38);
			this->passBox->TabIndex = 24;
			// 
			// pass_label
			// 
			this->pass_label->AutoSize = true;
			this->pass_label->ForeColor = System::Drawing::Color::White;
			this->pass_label->Location = System::Drawing::Point(815, 451);
			this->pass_label->Name = L"pass_label";
			this->pass_label->Size = System::Drawing::Size(125, 32);
			this->pass_label->TabIndex = 23;
			this->pass_label->Text = L":نام کاربری";
			// 
			// userBox
			// 
			this->userBox->Location = System::Drawing::Point(462, 378);
			this->userBox->Name = L"userBox";
			this->userBox->Size = System::Drawing::Size(329, 38);
			this->userBox->TabIndex = 22;
			// 
			// user_label
			// 
			this->user_label->AutoSize = true;
			this->user_label->ForeColor = System::Drawing::Color::White;
			this->user_label->Location = System::Drawing::Point(815, 383);
			this->user_label->Name = L"user_label";
			this->user_label->Size = System::Drawing::Size(180, 32);
			this->user_label->TabIndex = 21;
			this->user_label->Text = L":شماره دانشجویی";
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 14, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->label1->ForeColor = System::Drawing::Color::White;
			this->label1->Location = System::Drawing::Point(506, 135);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(253, 54);
			this->label1->TabIndex = 20;
			this->label1->Text = L"ثبت نام دانشجو";
			// 
			// textBox1
			// 
			this->textBox1->Location = System::Drawing::Point(462, 515);
			this->textBox1->Name = L"textBox1";
			this->textBox1->Size = System::Drawing::Size(329, 38);
			this->textBox1->TabIndex = 32;
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->ForeColor = System::Drawing::Color::White;
			this->label2->Location = System::Drawing::Point(815, 520);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(117, 32);
			this->label2->TabIndex = 31;
			this->label2->Text = L":رمز عبور";
			// 
			// StudentReg
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(16, 31);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->BackColor = System::Drawing::SystemColors::WindowFrame;
			this->ClientSize = System::Drawing::Size(1348, 842);
			this->Controls->Add(this->textBox1);
			this->Controls->Add(this->label2);
			this->Controls->Add(this->regbtn);
			this->Controls->Add(this->bckbtn);
			this->Controls->Add(this->lastNameBox);
			this->Controls->Add(this->last_name_label);
			this->Controls->Add(this->nameBox);
			this->Controls->Add(this->name_label);
			this->Controls->Add(this->passBox);
			this->Controls->Add(this->pass_label);
			this->Controls->Add(this->userBox);
			this->Controls->Add(this->user_label);
			this->Controls->Add(this->label1);
			this->Name = L"StudentReg";
			this->Text = L"StudentReg";
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
private: System::Void bckbtn_Click(System::Object^ sender, System::EventArgs^ e) {
	preForm->Show();
	this->Hide();
}
private: System::Void regbtn_Click(System::Object^ sender, System::EventArgs^ e) {
	if (false) { //age etelaat ok bood, sabte nam konim va berim be SuccessfulReg
		SuccessfulReg^ nextForm = gcnew SuccessfulReg(preForm);
		nextForm->Show();
		this->Hide();
	}
	else {
		UnsuccessfulReg^ nexForm = gcnew UnsuccessfulReg();
		nexForm->Show();
	}
}
};
}

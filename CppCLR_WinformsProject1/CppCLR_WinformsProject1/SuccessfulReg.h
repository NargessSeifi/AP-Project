﻿#pragma once

namespace CppCLR_WinformsProject1 {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Summary for SuccessfulReg
	/// </summary>
	public ref class SuccessfulReg : public System::Windows::Forms::Form
	{
	public:
		SuccessfulReg(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}
		SuccessfulReg(System::Windows::Forms::Form^ frm) {
			preForm = frm;
			InitializeComponent();
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~SuccessfulReg()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::Label^ label1;
	private: System::Windows::Forms::Button^ bckbtn;
	private: System::Windows::Forms::Form^ preForm;

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->bckbtn = (gcnew System::Windows::Forms::Button());
			this->SuspendLayout();
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 14, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->label1->ForeColor = System::Drawing::Color::White;
			this->label1->Location = System::Drawing::Point(102, 83);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(429, 54);
			this->label1->TabIndex = 0;
			this->label1->Text = L"ثبت نام با موفقیت انجام شد";
			// 
			// bckbtn
			// 
			this->bckbtn->BackColor = System::Drawing::SystemColors::WindowFrame;
			this->bckbtn->ForeColor = System::Drawing::Color::White;
			this->bckbtn->Location = System::Drawing::Point(123, 184);
			this->bckbtn->Name = L"bckbtn";
			this->bckbtn->Size = System::Drawing::Size(386, 60);
			this->bckbtn->TabIndex = 1;
			this->bckbtn->Text = L"بازگشت به صفحه ورود";
			this->bckbtn->UseVisualStyleBackColor = false;
			this->bckbtn->Click += gcnew System::EventHandler(this, &SuccessfulReg::bckbtn_Click);
			// 
			// SuccessfulReg
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(16, 31);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->BackColor = System::Drawing::Color::Maroon;
			this->ClientSize = System::Drawing::Size(650, 315);
			this->Controls->Add(this->bckbtn);
			this->Controls->Add(this->label1);
			this->Name = L"SuccessfulReg";
			this->Text = L"SuccessfulReg";
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
	private: System::Void bckbtn_Click(System::Object^ sender, System::EventArgs^ e) {
		preForm->Show();
		this->Hide();
	}
	};
}

﻿#pragma once
#include "StudentLogin.h"
#include "ManagerLogin.h"

namespace CppCLR_WinformsProject1 {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Summary for Form1
	/// </summary>
	public ref class Form1 : public System::Windows::Forms::Form
	{
	public:
		Form1(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~Form1()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::Button^ mngbtn;
	private: System::Windows::Forms::Button^ stdntbtn;
	protected:


	private: System::Windows::Forms::Label^ label1;
	protected:

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->mngbtn = (gcnew System::Windows::Forms::Button());
			this->stdntbtn = (gcnew System::Windows::Forms::Button());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->SuspendLayout();
			// 
			// mngbtn
			// 
			this->mngbtn->BackColor = System::Drawing::Color::Maroon;
			this->mngbtn->ForeColor = System::Drawing::Color::White;
			this->mngbtn->Location = System::Drawing::Point(408, 279);
			this->mngbtn->Name = L"mngbtn";
			this->mngbtn->Size = System::Drawing::Size(511, 113);
			this->mngbtn->TabIndex = 0;
			this->mngbtn->Text = L"پنل مدیریت";
			this->mngbtn->UseVisualStyleBackColor = false;
			this->mngbtn->Click += gcnew System::EventHandler(this, &Form1::mngbtn_Click);
			// 
			// stdntbtn
			// 
			this->stdntbtn->BackColor = System::Drawing::Color::Maroon;
			this->stdntbtn->ForeColor = System::Drawing::Color::White;
			this->stdntbtn->Location = System::Drawing::Point(408, 421);
			this->stdntbtn->Name = L"stdntbtn";
			this->stdntbtn->Size = System::Drawing::Size(511, 113);
			this->stdntbtn->TabIndex = 1;
			this->stdntbtn->Text = L"پنل دانشجو";
			this->stdntbtn->UseVisualStyleBackColor = false;
			this->stdntbtn->Click += gcnew System::EventHandler(this, &Form1::stdntbtn_Click);
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 14, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->label1->ForeColor = System::Drawing::Color::White;
			this->label1->Location = System::Drawing::Point(547, 142);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(232, 54);
			this->label1->TabIndex = 2;
			this->label1->Text = L"صفحه نخست";
			// 
			// Form1
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(16, 31);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->BackColor = System::Drawing::SystemColors::WindowFrame;
			this->ClientSize = System::Drawing::Size(1348, 842);
			this->Controls->Add(this->label1);
			this->Controls->Add(this->stdntbtn);
			this->Controls->Add(this->mngbtn);
			this->Name = L"Form1";
			this->Text = L"Form1";
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
	
	private: System::Void stdntbtn_Click(System::Object^ sender, System::EventArgs^ e) {
		StudentLogin^ nextForm = gcnew StudentLogin(this);
		nextForm->Show();
		this->Hide();
	}
	private: System::Void mngbtn_Click(System::Object^ sender, System::EventArgs^ e) {
		ManagerLogin^ nextForm = gcnew ManagerLogin(this);
		nextForm->Show();
		this->Hide();
	}
};
}
